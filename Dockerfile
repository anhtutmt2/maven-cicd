FROM tomcat:alpine
MAINTAINER Bin<tmtdtuctf@gmail.com>
RUN echo "export JAVA_OPTS=\"-Dapp.env=staging\"" > /usr/local/tomcat/bin/setenv.sh && rm -rf /usr/local/tomcat/webapps/ROOT
COPY ./target/WebApp.war /usr/local/tomcat/webapps/ROOT.war

EXPOSE 8080
CMD ["catalina.sh", "run"]
